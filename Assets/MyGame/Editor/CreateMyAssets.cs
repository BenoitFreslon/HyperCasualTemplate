using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CreateMyAssets
{
    [MenuItem ( "Assets/Create/Config Asset" )]
    public static void CreateMyConfigAsset ()
    {
        Config asset = ScriptableObject.CreateInstance<Config> ();

        AssetDatabase.CreateAsset ( asset, "Assets/MyGame/ScriptableObjects/Config.asset" );
        AssetDatabase.SaveAssets ();

        EditorUtility.FocusProjectWindow ();

        Selection.activeObject = asset;
    }
}