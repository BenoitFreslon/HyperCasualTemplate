using System;
using System.Collections;
using System.Collections.Generic;

using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // GameState actuel
    public static string State;

    [SerializeField] GameObject mainMenu;
    [SerializeField] GameObject hud;
    [SerializeField] GameObject gameOverScreen;



    // Start is called before the first frame update
    void Start ()
    {
        // Initialisation du jeu

        mainMenu.SetActive ( true );
        hud.SetActive ( false );
        gameObject.SetActive ( false );

        State = GameState.Start;

        startGame ();

    }

    void startGame ()
    {
        // Démarrage du jeu
        State = GameState.InGame;
        mainMenu.SetActive ( false );
        hud.SetActive ( true );
        BroadcastMessage ( "onStart" );
    }

    void Update ()
    {

#if UNITY_EDITOR
        if ( Input.GetKeyDown ( KeyCode.R ) ) {
            SceneManager.LoadScene ( SceneManager.GetActiveScene ().name );
        }
#endif
        if ( State != GameState.InGame ) return;
    }

    // Fin de la partie
    public void GameOver ()
    {
        Debug.Log ( "GameOver" );
        BroadcastMessage ( "onGameOver" );
        State = GameState.GameOver;
        DOTween.KillAll ();

        hud.SetActive ( false );
        gameOverScreen.SetActive ( true );
    }

}
