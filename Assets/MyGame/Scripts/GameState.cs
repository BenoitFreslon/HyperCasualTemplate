using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState
{
    static public string Start = "Start";
    static public string InGame = "InGame";
    static public string GameOver = "GameOver";
}