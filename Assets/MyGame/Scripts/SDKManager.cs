using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameAnalyticsSDK;
using Facebook.Unity;
using System;

#if UNITY_IPHONE || UNITY_EDITOR
using UnityEngine.iOS;
using Unity.Advertisement.IosSupport;
#endif

public class SDKManager : MonoBehaviour
{
    bool willTrackUser = false;
    // Start is called before the first frame update
    void Start ()
    {
        // iOS 14.5 Authorization Tracking
#if UNITY_IPHONE || UNITY_EDITOR
        Version minVersion = new Version ( "14.0" );
        Debug.Log ( "Device.systemVersion : " + Device.systemVersion );
        Version currentVersion = new Version ( Device.systemVersion ); // Parse the version of the current OS
        if ( currentVersion >= minVersion ) {
            InvokeRepeating ( "GetAuthorizationTrackingStatus", 0, 1 );
        } else {
            initSDK ( true );
        }
#elif UNITY_ANDROID || UNITY_EDITOR
        initSDK ( true );
#endif

    }

    void test ()
    {
        Debug.Log ( "test" );
    }
    void GetAuthorizationTrackingStatus ()
    {
#if UNITY_IPHONE || UNITY_EDITOR
        Debug.Log ( "GetAuthorizationTrackingStatus" );
        if ( ATTrackingStatusBinding.GetAuthorizationTrackingStatus () == ATTrackingStatusBinding.AuthorizationTrackingStatus.NOT_DETERMINED ) {
            ATTrackingStatusBinding.RequestAuthorizationTracking ();
        } else if ( ATTrackingStatusBinding.GetAuthorizationTrackingStatus () == ATTrackingStatusBinding.AuthorizationTrackingStatus.AUTHORIZED ) {
            initSDK ( true );
        } else if ( ATTrackingStatusBinding.GetAuthorizationTrackingStatus () == ATTrackingStatusBinding.AuthorizationTrackingStatus.NOT_DETERMINED ) {
            initSDK ( false );
        } else if ( ATTrackingStatusBinding.GetAuthorizationTrackingStatus () == ATTrackingStatusBinding.AuthorizationTrackingStatus.RESTRICTED ) {
            initSDK ( false );
        }
#endif
    }

    void initSDK ( bool trackUser )
    {
        CancelInvoke ( "GetAuthorizationTrackingStatus" );
        willTrackUser = trackUser;
        Debug.Log ( "Initialize" );
        GameAnalytics.Initialize ();
        Debug.Log ( "SetEnabledEventSubmission" );
        GameAnalytics.SetEnabledEventSubmission ( willTrackUser );
        Debug.Log ( "SetAdvertiserIDCollectionEnabled" );

        Debug.Log ( "Init Facebook" );
        // Facebook
        if ( !FB.IsInitialized ) {
            // Initialize the Facebook SDK
            FB.Init ( InitCallback, OnHideUnity );

        } else {
            // Already initialized, signal an app activation App Event
            Debug.Log ( "Facebook Already initialized" );
        }
    }

    private void InitCallback ()
    {
        // Facebook
        if ( FB.IsInitialized ) {
            // Signal an app activation App Event
            FB.ActivateApp ();
            FB.Mobile.SetAdvertiserIDCollectionEnabled ( willTrackUser );
            Debug.Log ( "FB.IsInitialized" );
            // Continue with Facebook SDK
            // ...
        } else {
            Debug.Log ( "Failed to Initialize the Facebook SDK" );
        }
    }

    private void OnHideUnity ( bool isGameShown )
    {
        if ( !isGameShown ) {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        } else {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

}
