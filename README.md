# HyperCasualTemplate
Hyper Casual game template source for Unity3D ready to develop.

**[GAME DEV LAUNCHER](https://videogamecreation.fr/devenir-developpeur-independant-de-jeux-video/)**

* Unity 2021.1.4f

##SDK

* Facebook SDK 9.1.2
* Game Anamytics 6.5.2

##Essential Packages

* Cinemachine
* DOTween
* Graphy
* Supercyan Free Sample Character Pack
* POLYGON Starter
* ProBuilder
* Joystick Pack
* Vibration
* Eazy Sound Manager
* Android Logcat
* Unity Recorder
* Etc.